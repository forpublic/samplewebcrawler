package com.bhasky;

import com.bhasky.crawler.CustomCrawler;
import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class WebCrawlerMain {
    final static Logger LOGGER = LoggerFactory.getLogger(WebCrawlerMain.class);

    public static void main(String[] args) {
        // Optional parameter : Number of concurrent threads : defaulted to 1
        int numberOfCrawlers = 1;
        if (args != null && args.length >= 2) {
            // Required parameter : Page that needs to crawled
            String pageUri = args[0];

            // Required parameter : Intermediate crawl data storage location
            String crawlStoragePath = args[1];
            if (args.length == 3) {
                numberOfCrawlers = Integer.parseInt(args[2]);
            }
            crawlWeb(pageUri, crawlStoragePath, numberOfCrawlers);
        } else {
            LOGGER.error("Invalid arguments...");
        }
    }

    public static void crawlWeb(String pageUri, String crawlStoragePath, int numberOfCrawlers) {
        try {
            File crawlStorage = new File(crawlStoragePath);

            // CrawlConfig - Can be used to set properties like depth of crawl. Default: No Limit
            CrawlConfig config = new CrawlConfig();
            config.setCrawlStorageFolder(crawlStorage.getAbsolutePath());

            PageFetcher pageFetcher = new PageFetcher(config);
            RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
            RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
            // Instantiate controller for this crawl
            CrawlController controller = new CrawlController(config, pageFetcher, robotstxtServer);
            controller.addSeed(pageUri);

            // The factory which creates instances of crawlers
            CrawlController.WebCrawlerFactory<CustomCrawler> factory = CustomCrawler::new;

            // Start the crawl.
            controller.start(factory, numberOfCrawlers);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }
}
