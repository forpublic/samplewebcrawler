package com.bhasky.crawler;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;
import java.util.regex.Pattern;

public class CustomCrawler extends WebCrawler {
    private final static Logger LOGGER = LoggerFactory.getLogger(CustomCrawler.class);

    // To exclude file types
    private final static Pattern FILTERS = Pattern.compile(".*(\\.(css|js|xml|gif|png|mp3|mp4|zip|gz|pdf))$");

    /*
    *   shouldVisit decides which URIs should be crawled
    *   This method receives two parameters:
    *       referringPage - Page in which we discovered the url
    *       url - new URL
    *   Here we are asking crawler to:
    *       Ignore uris that have FILTERS extensions
    *       Accept uri that starts with main domain
    * */
    @Override
    public boolean shouldVisit(Page referringPage, WebURL url) {
        String href = url.getURL().toLowerCase();
        String domain = url.getParentUrl().toLowerCase();
        return !FILTERS.matcher(href).matches() && href.startsWith(domain);
    }

    /*
    *   visit method gets called when page is fetched and ready to be processed.
    * */
    @Override
    public void visit(Page page) {
        if (page.getParseData() instanceof HtmlParseData) {
            HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();
            Set<WebURL> urlLinks = htmlParseData.getOutgoingUrls();

            // Print visited page's links
            String[] pageUrlArray = page.getWebURL().getPath().split("/");
            String childPageName;
            if (pageUrlArray.length == 0) {
                childPageName = "Main Page";
            } else {
                childPageName = pageUrlArray[pageUrlArray.length - 1];
            }
            System.out.println(new StringBuilder()
                    .append(childPageName)
                    .append(" : ")
                    .append(urlLinks.size())
                    .append(" links")
                    .toString());

            for (WebURL webURL : urlLinks) {
                System.out.println(new StringBuilder()
                        .append("\t")
                        .append(webURL.getURL().toLowerCase())
                        .toString());
            }
        }
    }
}
