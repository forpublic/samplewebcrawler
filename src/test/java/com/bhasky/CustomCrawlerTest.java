package com.bhasky;

import com.bhasky.crawler.CustomCrawler;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.url.WebURL;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;


class CustomCrawlerTest {

    private static final String PAGE_URL = "https://wiprodigital.com";

    static Stream<Arguments> dataProviderShouldVisit() {
        return Stream.of(
                Arguments.of(PAGE_URL, PAGE_URL, true),
                Arguments.of(PAGE_URL, "http://google.com", false),
                Arguments.of(PAGE_URL, "http://twitter.com", false),
                Arguments.of(PAGE_URL, "http://linkedin.com", false),
                Arguments.of(PAGE_URL, "http://facebook.com", false)
        );
    }

    private static WebURL createWebUrl(String uri) {
        WebURL webURL = new WebURL();
        webURL.setURL(uri);
        webURL.setParentUrl(PAGE_URL);
        return webURL;
    }

    @ParameterizedTest
    @MethodSource("dataProviderShouldVisit")
    public void testShouldVisit(String pageUri, String uri, boolean shouldVisit) throws Exception {
        CustomCrawler crawler = new CustomCrawler();
        Assertions.assertEquals(shouldVisit, crawler.shouldVisit(new Page(createWebUrl(pageUri)), createWebUrl(uri)));
    }
}