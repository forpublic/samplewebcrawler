# WebCrawler

Web Crawler was implemented on top of open source library Crawler4j. This application was implemented using java 1.8 and gradle as build tool. Prints visited and linked uris to console.  
## Table of content

- [Requirements](#requirements)
- [How-To](#how-to)
- [Further-Improvements](#Further-Improvements)

## Requirements

- Java JDK 1.8
- Gradle

## How To

Two ways to run this application.

### 1. Download SampleWebCrawler-1.0.0.0.jar from folder build/libs/ and run below command

- java -jar SampleWebCrawler-1.0.0.0.jar {uri} {storage} {concurrency}

    - uri - some uri that needs to be crawled
    - storage - {SOME-PATH}/crawler4j
    - concurrency - This is an optional integer field
    
### 2. Download the project

- Use choice of IDE
  - Build the project
  - To run the application, configure it with arguments

    - https://wiprodigital.com
    - {SOME-PATH}/crawler4j
    - {Some Integer} - This is an optional integer field
  
  - To test, configure the application with gradle test
- Use terminal/command prompt

  - run the command - gradle clean build
  - run - java -jar build/libs/SampleWebCrawler-1.0.0.0.jar {uri} {storage} {concurrency}
  - To test - gradle clean test
  
## Further Improvements

- Instead of writing to console, links can be added to a tree structure and present much better parent-child relationship.
- More tests can be written to cover much more code.
